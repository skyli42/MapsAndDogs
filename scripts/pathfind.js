var PF=require('pathfinding');
var exports=module.exports={};
exports.getPath=function (ptOne, ptTwo, map){
  var grid=new PF.Grid(map);
  var finder=new PF.DijkstraFinder();
  // console.log(map);
  return finder.findPath(ptOne.x, ptOne.y, ptTwo.x, ptTwo.y, grid);
}
exports.to2dArray=function(map, height, width){
  var map2d=[];
  // var arr = map;
  while(map.length){
  	var thing=map.splice(0,width);
  	map2d.push(thing);
  	// console.log(thing);
  }
  return map2d;
}
