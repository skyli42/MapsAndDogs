var Jimp=require('jimp')
var exports=module.exports={};
exports.processImage=function(map){
  var img=new Jimp(map, function(err, image){
    exports.image=img;
  });
  return img;
}
exports.toBinaryArr=function(hexarr){
  var binarr=[];
  for(var i in hexarr){
    binarr.push(hexarr[i]==255?1:0);
  }
  return binarr;
}
