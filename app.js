var express = require('express'), stylus = require('stylus'), nib = require('nib');
var http = require('http');
var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);
var imageProcess = require('./scripts/imageProcess');
var pathfind=require('./scripts/pathfind')
var mapjson=require('./scripts/maps');
function compile(str, path) { //compiles stylus into css
  return stylus(str).set('filename', path).use(nib());
}

app.set('views', __dirname + '/templates')
app.set('view engine', 'jade')
app.use(stylus.middleware({
  src: __dirname + '/public/stylus',
  dest:__dirname + '/public/stylus',
  compile: compile
}))
console.log(__dirname);

app.use('/bower', express.static('bower_components'));
app.use('/static', express.static('public'))
// app.use(express.static(__dirname + '/public'));

// io.on('connection', function(socket) {
//   socket.on('request', function(msg) {
//     var abfloor1=mapjson.ab.floor_one;
//     io.emit('return', pathfind.getPath({x:150, y:619}, {x:210, y:689}, pathfind.to2dArray(abfloor1, 779, 1000)));
//     // io.emit('return', pathfind.to2dArray(abfloor1, 779, 1000));
//   });
// });

//serving different pages + server listening
app.get('/', function(req, res) { //index
  res.render('index', {
    title: 'Home'
  })
});
app.get('/about', function(req, res) {
  res.render('about', {
    title: 'About'
  })
});
app.get('/map', function(req, res){
  res.render('map', {
    title: 'Map'
  })
});
app.get('/people', function(req, res){
  res.render('people', {
    title: 'People'
  })
});
server.listen(app.listen(process.env.PORT || 3000, function() {
  var host = server.address().address;
  var port = server.address().port;
  console.log('Listening at http://%s:%s', host, port);
}));
